FROM ubuntu:20.04

LABEL version="1"\
      os="arm64 ubuntu 20.04"\
      gcc="11.2.0"\
      glibc="2.31"\
      cmake="3.30.6"

WORKDIR /tmp/
COPY . ./
WORKDIR /tmp/files

ENV DEBIAN_FRONTEND=noninteractive

RUN ./init.arm64.sh && rm -rf /tmp/files

WORKDIR /root
