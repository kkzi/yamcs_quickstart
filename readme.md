
## 1. Build gcc-11.2 with ubuntu 20.04
```shell
curl -L https://t.im/KIEku | bash
```

## 2. Wait
```
Setting up manpages (5.05-1) ...
Setting up binutils-common:arm64 (2.34-6ubuntu1.9) ...
Setting up linux-libc-dev:arm64 (5.4.0-204.224) ...
Setting up libctf-nobfd0:arm64 (2.34-6ubuntu1.9) ...
Setting up libcrypt-dev:arm64 (1:4.4.10-10ubuntu4) ...
Setting up libbinutils:arm64 (2.34-6ubuntu1.9) ...
Setting up libc-dev-bin (2.31-0ubuntu9.16) ...
Setting up libctf0:arm64 (2.34-6ubuntu1.9) ...
Setting up manpages-dev (5.05-1) ...
Setting up binutils (2.34-6ubuntu1.9) ...
Setting up libc6-dev:arm64 (2.31-0ubuntu9.16) ...
Setting up gcc-11-ubuntu-18.04 (11.2.0-1) ...
Processing triggers for libc-bin (2.31-0ubuntu9.14) ...
Removing intermediate container 341defc4df65
 ---> 65399f87e54f
Step 8/8 : WORKDIR /root
 ---> Running in 3cc63be44207
Removing intermediate container 3cc63be44207
 ---> 6f7e1fad6e74
Successfully built 6f7e1fad6e74
Successfully tagged udev20:arm64_1
```

## 3. Check image
```
docker images | less

REPOSITORY 	TAG           IMAGE ID       CREATED          SIZE
udev20          arm64_1       6f7e1fad6e74   14 minutes ago   959MB

#or

REPOSITORY 	TAG           IMAGE ID       CREATED          SIZE
udev20          x64_1         6f20c641609d   12 minutes ago   1.21GB

```

## 4. Run container
```
docker run -it --name u20 --rm udev20:x64_1 zsh

# or 

docker run -it --name u20 --rm udev20:arm64_1 zsh
```

## 5. Requirement
```
curl
git
docker
```

## 6. Detail
```
➜  ~ gcc --version
gcc (GCC) 11.2.0
Copyright (C) 2021 Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

➜  ~ g++ --version
g++ (GCC) 11.2.0
Copyright (C) 2021 Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

➜  ~ cmake --version
cmake version 3.30.6

CMake suite maintained and supported by Kitware (kitware.com/cmake).
➜  ~ ninja --version
1.10.0
➜  ~ git --version
git version 2.25.1
```
