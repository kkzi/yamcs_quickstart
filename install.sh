#!/bin/bash

VER=1
DIR=/tmp/dev.$RANDOM

git clone --depth=1 https://gitee.com/kkzi/dev.git $DIR 
cd $DIR 

get_arch=`arch`
if [[ $get_arch == "x86_64" ]];then
    docker build -t udev20:x64_$VER -f udev20.x64-linux.Dockerfile .
elif [[ $get_arch == "aarch64" ]];then
    docker build -t udev20:arm64_$VER -f udev20.arm64-linux.Dockerfile .
else
    echo "Unsupported arch $get_arch"
fi

rm -rf $DIR

