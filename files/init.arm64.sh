sed -i -e 's@//ports.ubuntu.com/\? @//ports.ubuntu.com/ubuntu-ports @g' -e 's@//ports.ubuntu.com@//mirrors.ustc.edu.cn@g' /etc/apt/sources.list
apt-get update 
apt-get install -y tmux zsh vim git make autoconf automake curl wget unzip zip ninja-build pkg-config net-tools apt-utils
apt-get install -y ./gcc-11.2.0-1-ubuntu-18.04-arm64.deb && ln -s /usr/bin/gcc-11 /usr/bin/gcc && ln -s /usr/bin/g++-11 /usr/bin/g++
ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime && echo 'Asia/Shanghai' >/etc/timezone 
tar -zxf cmake-3.30.6-linux-aarch64.tar.gz -C /usr/local/ && ln -s /usr/local/cmake-3.30.6-linux-aarch64/bin/cmake /usr/local/bin/cmake
unzip -q ohmyzsh-master.zip && mv ohmyzsh-master ~/.oh-my-zsh && cp ~/.oh-my-zsh/templates/zshrc.zsh-template ~/.zshrc && chsh -s $(which zsh)
unzip -q vim-main.zip && mv .vim-main ~/.vim
rm -rf /var/lib/apt/lists/* 




    


